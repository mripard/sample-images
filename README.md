# Automotive sample images

This is the git repository for the CentOS Automotive SIG sample images
working with osbuild and other tools. These images are considered 'proof
of concept' images for how parties can work with the CentOS Automotive SIG
repositories and tools.

Documentation for making images are included in the
`osbuild-manifests/Makefile` using either `make` or `make list-targets`

Further information about this repository is kept up to date in the
Automotive SIG documentation website. For more information, visit:

* [Automotive SIG wiki page](https://wiki.centos.org/SpecialInterestGroup/Automotive)
* [Automotive SIG documentation](https://sigs.centos.org/automotive)


Other important websites:
* [Image Builder](https://www.osbuild.org/)
* [Image Builder Documentation](https://www.osbuild.org/guides/introduction.html)
* [Minimum Automotive Contents](https://tiny.distro.builders/workload-overview--automotive-workload-in--automotive-repositories-c9s.html)
  
  
